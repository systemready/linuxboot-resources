# RPi4 LinuxBoot Guide
---

## Table of contents 

[[_TOC_]]



![Demo Video](DemoMedia/RPi4fedora.mov)

## 1 Background

### 1.1 About this Guide
The following guide outlines how to create a UEFI+LinuxBoot Firmware image for the Raspberry Pi from scratch. This guide may be useful for porting LinuxBoot to other systems, but every system will have its own system specific details and/or quirks to work around.


**Known Limitations**
This is an early prototype implementation, so there are some real limitations.
 
1. Currently this image is not size/space optimized, final firmware is ~10MB.
2. Does not yet remove unused EDK2 DXEs
3. Not all non-volatile variables are being preserved correctly. 


## 2 System Details and Requirements 

### 2.1 System Details 
	
These details serve as an outline of the systems upon which this build process was validated.
	
AArch64:

	- < 10GB of disk space available 
	- 32GB of ram
	- 16 threads
	- Native AArch64 build system
	- Debian 10 Buster, Kernel 4.19.0
	- GCC 8.3.0-6
	- Go 1.16.6
	

x86_64:

	- < 10GB of disk space available
	- 8GB of ram
	- 8 threads
	- Docker x86_64 Image
	- Docker Ubuntu 18.04 LTS
	- gcc-linaro-6.2.1-2016.11-x86_64_aarch64-linux-gnu cross compiler
	- Go 1.16.6


<details>
<summary>Click to show APT packages installed:</summary>

	- uuid
	- uuid-dev
	- build-essential
	- git
	- xz-utils
	- wget
	- python3
	- python
	- acpi
	- acpica-tools
	- flex
	- bison
	- bc

</details>


**RPi4 Setup:**

	- RPi4 B 4GB
	- 4GB+ microSD Card
	- RPi4 Serial UART over USB


## 3. Software Setup

### 3.1 EDK2 
This section will serve as a condensed guide to set up and build EDK2 Firmware.
A full guide can be found from the [EDK2-Platform Repo](https://github.com/tianocore/edk2-platforms).

**x86_64 Cross Compiler**

If on x86_64, download a cross compiler from [Linaro](http://releases.linaro.org/components/toolchain/binaries/6.2-2016.11/aarch64-linux-gnu/):

```
$ mkdir linaro;cd linaro
$ wget http://releases.linaro.org/components/toolchain/binaries/6.2-2016.11/aarch64-linux-gnu/gcc-linaro-6.2.1-2016.11-x86_64_aarch64-linux-gnu.tar.xz
$tar -xvf gcc-linaro-6.2.1-2016.11-x86_64_aarch64-linux-gnu.tar.xz
``` 

then 

`export PATH=$PATH:<path/to/gcc-linaro-6.2.1-2016.11-x86_64_aarch64-linux-gnu/bin/>`


#### 3.1.1 Setting Up EDK2

Create an EDK2 workspace:

```
$ mkdir -p <path to working directory>/tianocore
$ cd tianocore
$ export WORKSPACE=$PWD
```

Clone EDK2 and dependencies:

```
$ git clone https://github.com/tianocore/edk2.git
$ cd edk2; git submodule update --init; cd -
...
$ git clone https://github.com/tianocore/edk2-platforms.git
$ cd edk2-platforms; git submodule update --init; cd -
...
$ git clone https://github.com/tianocore/edk2-non-osi.git
```

Setup package path:

```
$ export PACKAGES_PATH=$PWD/edk2:$PWD/edk2-platforms:$PWD/edk2-non-osi
```

Set up build environment:

```
$ . edk2/edksetup.sh
```

Build BaseTools:

```
$ make -C edk2/BaseTools
```


**Build Environment Setup when Returning**

If you've already cloned and built the BaseTools, but need to re-setup the environment, simply re-export your variables and run the `edk2setup.sh` script. 

```
$ cd <path to working directory>/tianocore
$ export WORKSPACE=$PWD
$ export PACKAGES_PATH=$PWD/edk2:$PWD/edk2-platforms:$PWD/edk2-non-osi
$ . edk2/edksetup.sh
```

If on x86_64, export your cross compiler
`export PATH=$PATH:<path/to/gcc-linaro-6.2.1-2016.11-x86_64_aarch64-linux-gnu/bin/>`

#### 3.1.3 Testing EDK2 Setup & Building 

EDK2 can be *very* particular sometimes, so before modifying the code make sure you can build a working image.

Optionally to build in parallel, run the following command and reference `NUM_CPUS` when you build:

```
NUM_CPUS=$((`getconf _NPROCESSORS_ONLN` + 2))
```


To build on AArch64:

```
$ build -n $NUM_CPUS -a AARCH64 -b DEBUG -t GCC5 -p Platform/RaspberryPi/RPi4/RPi4.dsc
```

To build on x86_64: Add `GCC5_AARCH64_PREFIX=aarch64-linux-gnu- ` in front of the build command.

```
GCC5_AARCH64_PREFIX=aarch64-linux-gnu-  build -n $NUM_CPUS -a AARCH64 -b DEBUG -t GCC5 -p Platform/RaspberryPi/RPi4/RPi4.dsc
```


A quick breakdown of the build command:

```
    -n Number of CPUS
    -a Build Architecture 
    -b Build type <DEBUG| RELEASE>
    -t Toolchain version, GCC5 is really GCC5+, in our case GCC 8.3
    -p Build platform, the RaspberryPi4.
```

The resulting firmware image can be found in `$WORKSPACE/BUILD/RPi4/DEBUG_GCC5/FV/RPI_EFI.fd`. The `DEBUG_GCC5` directory will change if you use a different toolchain version or build type such as `RELEASE`.



### 3.2 Trusted-Firmware-A (TFA)
TFA is the second stage in our boot flow, which then jumps to UEFI. There is no need to modify the TFA source code, but for an RPi4 work around you will need to rebuild it with an updated command line option later on.

**Downloading and Test Building TFA**

These instructions are based on the [RPi4 EDK2 Plat readme](https://github.com/tianocore/edk2-non-osi/blob/master/Platform/RaspberryPi/RPi4/TrustedFirmware/Readme.md).

On AArch64:

```
$ cd <path to working directory>/
$ git clone https://git.trustedfirmware.org/TF-A/trusted-firmware-a.git/
$ cd trusted-firmware-a
trusted-firmware-a$ make PLAT=rpi4 RPI3_PRELOADED_DTB_BASE=0x1F0000 PRELOADED_BL33_BASE=0x20000 SUPPORT_VFP=1 DEBUG=0 all
```

On x86_64: Add `CROSS_COMPILE=aarch64-linux-gnu-` into the build flags

```
$ cd trusted-firmware-a
$ make realclean
trusted-firmware-a$ make CROSS_COMPILE=aarch64-linux-gnu- PLAT=rpi4 RPI3_PRELOADED_DTB_BASE=0x1F0000 PRELOADED_BL33_BASE=0x20000 SUPPORT_VFP=1 DEBUG=0 all
```

This will generate a `bl31.bin` in the `trusted-firmware-a` directory. Make note, you will need to rebuild and use this image later.

### 3.3 u-root initramfs

u-root is a Golang powered initramfs/busybox style set of user tools used by LinuxBoot.

#### 3.3.1 Setting up Go

If you don't have Go, you can download it [from here](https://golang.org/dl/). Once you've downloaded and set up Go, test it by running `go version`. You should see a message similar to: `version go1.16.6 linux/arm64`.
 
Set a `GOPATH`:

```
export GOPATH=$HOME/go
export PATH=$PATH:$GOROOT/bin:$GOPATH/bin

```

#### 3.3.2 Download & Build u-root
Once you have Go, use it to download u-root. (This is condensed from [u-root github](https://github.com/u-root/u-root) Make note of `GO11MODULE=off`.)

```
go$ GO111MODULE=off go get github.com/u-root/u-root
```

Once u-root has downloaded, use it to build a basic initramfs.

```
go$ GO111MODULE=off GOARCH=arm64 u-root core
```

This will only include `core` utils. You can modify this to build more utils if desired. Once built, it will tell you where the the initramfs is located.

`<path to>/root.initramfs.linux_arm64.cpio`

### 3.4 Linux Setup & Building
In order to compile the LinuxBoot image, clone and compile the Linux kernel. This guide was tested with Linux kernel version 5.14.

#### 3.4.1 Download and Setup Linux

Clone and checkout v5.14 of the Linux kernel:

```
$ git clone https://github.com/torvalds/linux
$ cd linux
linux$ git checkout v5.14

```

Set up the defconfig for Linuxboot. 
[defconfig Linux patch here](Code/0001-adding-Basic-LinuxBoot-defconfig.patch)

**NOTE:** This is not a minimal/size optimized defconfig for this initial proof of concept.
  

Download & apply the patch in the linux repo.

```
$cd <path to>/linux
linux$ get apply <path to>0001-adding-Basic-LinuxBoot-defconfig.patch

```

This should put the defconfig in the right sub-directory,`<path to>/linux/arch/arm64/configs/linuxboot_defconfig`. 

Update the path to the initramfs in the defconfig by updating the variable `CONFIG_INITRAMFS_SOURCE` to point to the initramfs location that was given during the build process in 3.3.2.

`linuxboot_defconfig`

```
...
CONFIG_INITRAMFS_SOURCE= <path to >/root.initramfs.linux_arm64.cpio
...
```

#### 3.4.2 Building Linux

on AArch64:

```
linux$ mkdir -p out/linuxboot
linux$ make mrproper
linux$ make O=out/linuxboot/ ARCH=arm64 linuxboot_defconfig
linux$ make O=out/linuxboot/ -j20 Image
```

on x86_64:

```
linux$ mkdir -p out/linuxboot
linux$ make mrproper
linux$ make O=out/linuxboot/ CROSS_COMPILE=aarch64-linux-gnu- ARCH=arm64 linuxboot_defconfig
linux$ make O=out/linuxboot/ CROSS_COMPILE=aarch64-linux-gnu- ARCH=arm64 -j20 Image
```

The `-j 20` flag enables building in 20 parallel threads, adjust this flag to suit your system.


You should end up with an image located at `linux/out/linuxboot/arch/arm64/boot/Image`; you will use this image to integrate LinuxBoot into UEFI.



## 4. Code Changes

This guide provides a patch with instructions to get LinuxBoot working, as well as a longer detailed explanation of all the code changes.

### 4.0 EDK2 Patch

Nearly all the needed changes are included in the EDK2 Platform patch. [0001-Platform-RaspberryPi-RPi4-Added-LinuxBoot.patch](Code/0001-Platform-RaspberryPi-RPi4-Added-LinuxBoot.patch)

After you've applied the patch in `edk2-platforms`, you need to copy the Linux image created in 3.4.3 into the correct location.

```
$ edk2-platforms/Platform/RaspberryPi$ mkdir -p LinuxBootPkg/AArch64
$ cp <path to>/linux/out/linuxboot/arch/arm64/boot/Image <path to>/edk2-platforms/Platform/RaspberryPi/LinuxBootPkg/AArch64/Image
```

Once the patch is applied and the image has been copied, you can move onto section 5.0, however, detailed instructions of the changes made in the patch are below.


### 4.1 Detailed Instructions for EDK2

These detailed instructions sections are for reference, as the changes are included in the patch in section 4.0.

#### 4.1.1 Adding LinuxBootPkg (Detailed Instructions)

Create a LinuxBoot UEFI application package to be used in the EDK2 build.

Add a platform sub-directory to house the package inf, and the LinuxBoot image.

```
$ cd <path to>/edk2-platforms/Platform/RaspberryPi
edk2-platforms/Platform/RaspberryPi$ mkdir -p LinuxBootPkg/AArch64`
edk2-platforms/Platform/RaspberryPi$ cd LinuxBootPkg
```

In the LinuxBootPkg directory create `linuxboot.inf` filled with the following; 

`edk2-platforms/Platform/RaspberryPi/LinuxBootPkg/linuxboot.inf`

```
[Defines]
    INF_VERSION             = 0x00010006
    BASE_NAME               = LinuxBoot
    FILE_GUID               = 7C04A583-9E3E-4f1c-AD65-E05268D0B4D1
    MODULE_TYPE             = UEFI_APPLICATION
    VERSION_STRING          = 1.0

[Binaries.AArch64]
    PE32|AArch64/Image|*
```

The LinuxBoot image built in Section 3.4.2 gets put into the `AArch64` sub-directory

`cp <path to>/linux/out/linuxboot/arch/arm64/boot/Image <path to>/edk2-platforms/Platform/RaspberryPi/LinuxBootPkg/AArch64/Image`



#### 4.1.2 Modifying RPi4 Build Platform (Detailed Instructions)

With LinuxBootPkg created and placed locally in the RaspberryPi platform folder, the next step is to substitute the UEFI shell for the LinuxBoot image, as well as create some extra room for the LinuxBoot image in the UEFI image.

**Limitations**

The RPi4 has a Device Tree Blob (DTB) that gets loaded into the memory address `0x001f0000` by the first stage boot loader, limiting space for TFA and UEFI to 2MB. Normal UEFI fits in this space, but this PoC UEFI + LinuxBoot does not. It may be possible to squeeze into this space, but for the time being this guide will move the UEFI image + LinuxBoot image behind after this address, and then extend its size.

**RPi4LinuxBoot.fdf**
 
Duplicate the `RPi4.fdf` into a new `RPi4LinuxBoot.fdf`.
This PoC simply swaps the LinuxBoot image in place of the standard UEFI shell. To do this modify the new  `RPi4LinuxBoot.fdf` which defines the size of the UEFI image and what is contained inside.

Around line 232 for RPi4 swap the LinuxBootPkg in place of the UEFI shell.

	`INF edk2-platforms/Platform/RaspberryPi/LinuxBootPkg/linuxboot.inf`

Around line 30, adjust the total image size to 10MB. This is arbitrarily large for this PoC.

	`NumBlocks     = 0xA4`


The UEFI image won't fit between TF-A and the aforementioned DTB, so shift the UEFI image behind the DTB, increase its size, and adjust all the addresses behind it. This starts around line 50 for the RPi4.

```
  #
  # UEFI image
  #
+ 0x00200000|0x00820000
  gArmTokenSpaceGuid.PcdFvBaseAddress|gArmTokenSpaceGuid.PcdFvSize
```

<details>
<summary>Click to show full diff details</summary>
`RPi4LinuxBoot.fdf`

```
...
  #
  # UEFI application (Shell Embedded Boot Loader)
  #
- INF ShellPkg/Application/Shell/Shell.inf
+ INF edk2-platforms/Platform/RaspberryPi/LinuxBootPkg/linuxboot.inf
...

  [FD.RPI_EFI]
  BaseAddress   = 0x00000000|gArmTokenSpaceGuid.PcdFdBaseAddress
- Size          = 0x001f0000|gArmTokenSpaceGuid.PcdFdSize
+ Size          = 0x00A40000|gArmTokenSpaceGuid.PcdFdSize

  ErasePolarity = 1

  BlockSize     = 0x00001000|gRaspberryPiTokenSpaceGuid.PcdFirmwareBlockSize
- NumBlocks     = 0x1f0
+ NumBlocks     = 0xA4
...
  #
  # ATF primary boot image
  #
  0x00000000|0x00020000
  FILE = $(TFA_BUILD_BL31)

+ #
+ # This is just for documentation purposes! The DTB reserved space is not part of the FD,
+ # but this is exactly where it is expected to be.
+ #
+ # 0x001f0000|0x10000
+ # gRaspberryPiTokenSpaceGuid.PcdFdtBaseAddress|gRaspberryPiTokenSpaceGuid.PcdFdtSize
+ #

  #
  # UEFI image
  #
+ 0x00200000|0x00820000
  gArmTokenSpaceGuid.PcdFvBaseAddress|gArmTokenSpaceGuid.PcdFvSize
  FV = FVMAIN_COMPACT

  # 
  # Variables (0x20000 overall).
  # ...
  #

  # NV_VARIABLE_STORE
+ 0x00A20000|0x0000e000
  gRaspberryPiTokenSpaceGuid.PcdNvStorageVariableBase|gEfiMdeModulePkgTokenSpaceGuid.PcdFlashNvStorageVariableSize
 
  DATA = {
    ...
  }

  # NV_EVENT_LOG
+ 0x00A2e000|0x00001000
  gRaspberryPiTokenSpaceGuid.PcdNvStorageEventLogBase|gRaspberryPiTokenSpaceGuid.PcdNvStorageEventLogSize

  # NV_FTW_WORKING header
+ 0x00A2f000|0x00001000
  gRaspberryPiTokenSpaceGuid.PcdNvStorageFtwWorkingBase|gEfiMdeModulePkgTokenSpaceGuid.PcdFlashNvStorageFtwWorkingSize

  DATA = {
   ...
  }

  # NV_FTW_WORKING data
+ 0x00A30000|0x00010000
  gRaspberryPiTokenSpaceGuid.PcdNvStorageFtwSpareBase|gEfiMdeModulePkgTokenSpaceGuid.PcdFlashNvStorageFtwSpareSize
  
- #
- # This is just for documentation purposes! The DTB reserved space is not part of the FD,
- # but this is exactly where it is expected to be.
- #
- # 0x001f0000|0x10000
- # gRaspberryPiTokenSpaceGuid.PcdFdtBaseAddress|gRaspberryPiTokenSpaceGuid.PcdFdtSize
- #
```

</details>




**RPi4LinuxBoot.dsc**

Duplicate `RPi4.dsc` into a new `RPi4LinuxBoot.dsc` and update the sourced fdf file to the new `RPi4LinuxBoot.fdf`. 

Since the UEFI image was moved to `0x00200000`, you will need to build a new TFA image to jump to `0x00200000`. In order to do this, update which TF-A(BL31) image is included (which will be built later). Lastly, move the system memory address to make room for the new extra large UEFI image. 

`RPi4LinuxBoot.dsc`


```
...
  BUILD_TARGETS                  = DEBUG|RELEASE|NOOPT
  SKUID_IDENTIFIER               = DEFAULT
- FLASH_DEFINITION               = Platform/RaspberryPi/$(PLATFORM_NAME)/$(PLATFORM_NAME).fdf
+ FLASH_DEFINITION               = Platform/RaspberryPi/$(PLATFORM_NAME)/$(PLATFORM_NAME)LinuxBoot.fdf  
...
  # on the firmware, being able to use a local TF-A build without extra copy
  # operations ends up being very helpful.
  #
-  DEFINE TFA_BUILD_BL31 = $(TFA_BUILD_ARTIFACTS)/bl31.bin
+  DEFINE TFA_BUILD_BL31 = $(TFA_BUILD_ARTIFACTS)/bl31_lb.bin
  !endif
...
  # This matches PcdFvBaseAddress, since everything less is the FD, and
  # will be reserved away.
  #
- gArmTokenSpaceGuid.PcdSystemMemoryBase|0x00200000
- gArmTokenSpaceGuid.PcdSystemMemorySize|0x3fe00000
+  gArmTokenSpaceGuid.PcdSystemMemoryBase|0x00A40000
+  gArmTokenSpaceGuid.PcdSystemMemorySize|0x3F5C0000
``` 


---

## 5. Building Final Image


### 5.1 Building TF-A
Now that the LinuxBoot image is in a UEFI package, and the needed changes to our EDK2 platform are complete, the last step is to rebuild TF-A with the new UEFI(BL33) location `0x00200000`.

AArch64:

```
$ cd trusted-firmware-a
trusted-firmware-a$ make realclean
trusted-firmware-a$ make PLAT=rpi4 RPI3_PRELOADED_DTB_BASE=0x1F0000 PRELOADED_BL33_BASE=0x00200000 SUPPORT_VFP=1 DEBUG=0 all
```

x86_64: add `CROSS_COMPILE=aarch64-linux-gnu-` into the build flags:

```
$ cd trusted-firmware-a
$ make realclean
trusted-firmware-a$ make CROSS_COMPILE=aarch64-linux-gnu- PLAT=rpi4 RPI3_PRELOADED_DTB_BASE=0x1F0000 PRELOADED_BL33_BASE=0x00200000 SUPPORT_VFP=1 DEBUG=0 all
```

Copy `bl31.bin` into `edk2-non-osi/Platform/RaspberryPi/RPi4/TrustedFirmware/bl31_lb.bin`, make sure not squash the existing TF-A image. 



### 5.2 Building UEFI
Build EDK2 using the new .dsc file

AARCH64:

`build -n $NUM_CPUS -a AARCH64 -b DEBUG -t GCC5 -p Platform/RaspberryPi/RPi4/RPi4LinuxBoot.dsc`

x86_64: Add `GCC5_AARCH64_PREFIX=aarch64-linux-gnu- ` in front of the build command.

`GCC5_AARCH64_PREFIX=aarch64-linux-gnu- build -n $NUM_CPUS -a AARCH64 -b DEBUG -t GCC5 -p Platform/RaspberryPi/RPi4/RPi4LinuxBoot.dsc`

The final firmware image should be found in `$WORKSPACE/Build/RPi4/DEBUG_GCC5/FV/RPI_EFI.fd`



## 6. Booting

### 6.1 Setting up SD Card
Once you've built your new EDK2 image, download a zip from [https://github.com/pftf/RPi4/releases](https://github.com/pftf/RPi4/releases), unzip and place the contents onto an empty microSD card. Next copy the FD from `$WORKSPACE/Build/RPi4/DEBUG_GCC5/FV/RPI_EFI.fd` and use it to replace the `RPI_EFI.fd` on the microSD card. 

### 6.2 Setting up OS Media
Please download and install or burn a Linux disk image onto your primary boot drive.


#### 6.2.1 Fedora
This guide used a Fedora Server 34 AArch64 **Raw** image, which can be [downloaded here](https://getfedora.org/server/download/).

Once downloaded, you can restore the image onto a USB flash drive and insert it into the RPi4.


### 6.3 Booting UEFI & u-root

Once the RPi4 is turned on, repeatedly press the `esc` key until the UEFI menu appears.
![Press esc!](DemoMedia/Images/BootUEFI.png)

Next, select `Boot Manager`, and select `UEFI Shell` to start LinuxBoot.

![UEFI Menu](DemoMedia/Images/BootManager.png)

![Shell is actually LinuxBoot](DemoMedia/Images/UEFIShell.png)
 
### 6.4 Booting Into Final OS


After you've started LinuxBoot, it may take a minute for the kernel to fully load. 
![Welcome to u-root!](DemoMedia/Images/u-root.png)


Once u-root is running, you can list the disks with `ls /dev/sd*`.

Create a folder and mount the appropriate disk(s) `mkdir fedora; mount sda2 fedora`.
 Once mounted, configure kexec with `kexec -i <initrd> -l <VmLinux> --append=root=<root path>` Afterwards, execute with `kexec -e` 
![Setup and Kexec](DemoMedia/Images/kexec.png)

If everything is set up properly, it will boot into the final OS.

![Welcome to Fedora](DemoMedia/Images/FedoraLogin.png)



## 7. Future Work (WIP)

-  u-root initramfs 
	- Add more tools into initramfs image
- LinuxBoot Boot Manager 
	- Finish implementation of LinuxBootBootManager
	![](DemoMedia/Images/LinuxBootBootManager1.png)

- Slim Down Firmware
	- Removing un-needed drives/DXE's
	- Look into remedying DTB location issues on RPi4
	- [Compress image](https://github.com/u-root/u-root#compression)
	- Reduce kernel configuration