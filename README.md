# LinuxBoot proof of concepts

---


## [SBSA QEMU LinuxBoot](SBSA-QEMU/)
The sbsa-ref board intends to look like real hardware. The Server Base System Architecture defines a minimum base line of hardware support and importantly how the firmware reports that to any operating system. It is intended to be a machine for developing firmware and testing standards compliance with operating systems.

## [RaspberryPi4 LinuxBoot](RPi4/)
The RaspberryPi4 is an easily accessible, AArch64 single board computer. With quad cores, SBBR (ACPI+UEFI) firmware compliance, and a $35 starting price, its accessibility as real hardware, is unmatched. The RPi4 is a great system for exploration and development both firmware and software, especially when real hardware is a must.

## [Arm Neoverse FVP LinuxBoot](https://gitlab.arm.com/arm-reference-solutions/arm-reference-solutions-docs/-/blob/21bdaf7141571004f8c138bc25b334d01e9d9b5e/docs/infra/common/linuxboot.rst)

Fixed Virtual platforms are complete simulations of an Arm system, including processor, memory and peripherals. These are set out in a "programmer's view", which gives you a comprehensive model on which to build and test your software.



```
  +------------------------+
  |                        |
  |    Trusted Firmware    |
  |                        |
  +-----------+------------+
              |
              |
              v                                                +------------------------+              +----------------------+
  +------------------------+                                   |                        |              |                      |
  |                        |       launch stage-1 kernel       |  Stage-1 Linux Kernel  |  kexec call  |                      |
  | Minimal UEFI DXE phase +---------------------------------->|          +             +------------->|  Final Linux Kernel  |
  |                        |     as UEFI shell application     |        u-root          |              |                      |
  +------------------------+                                   |                        |              |                      |
                                                               +------------------------+              +----------------------+
```