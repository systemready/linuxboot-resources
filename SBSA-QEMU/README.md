# SBSA QEMU LinuxBoot Guide
---

## Table of contents 

[[_TOC_]]

   
    
![Demo: Replacing UEFI shell](DemoMedia/ReplaceUefiShell/LinuxbootOnSbsaqemuDemo.webm)

![Demo: Using Platform Boot Manager and `uinit`](DemoMedia/PlatformBootMgrDemo/automaticRun.webm)


## 1 Background

### 1.1 What is this guide?
The following guide outlines how to create a UEFI+LinuxBoot firmware image for the sbsa-ref QEMU machine from scratch. It relies on the scripts [`ImageBuildscript.sh`](Code/scripts/ImageBuildscript.sh) and [`Edk2Buildscript.sh`](Code/scripts/Edk2Buildscript.sh) while detailing the extra steps and information needed to run them successfully. 

Help to improve this guide! Any comments or questions contact Frances Conboy at frances.conboy@arm.com.

**Note:** This guide may be useful to for porting to other systems, but every system will have its own system specific details and or quirks to work around.

### 1.2 Known Limitations
1. Currently this image is not size or space optimized
2. Does not (yet) remove unused EDK2 DXEs

## 2 System Details
	
These system details purely serve as an outline of the systems this build process was validated on, they are not requirements.

x86_64:

	- 64 GB of RAM
	- 56 threads
	- Ubuntu 18.04 LTS
	- gcc-linaro-6.2.1-2016.11-x86_64_aarch64-linux-gnu cross compiler
	- Go 1.14
	- QEMU emulator version 6.1.50 (v6.1.0-1446-g4d1a525dfa)

AArch64:

    - 256 GB of RAM
    - 224 threads
    - Fedora Linux 36 (Server Edition)
    - GCC 12.1.1 (Red Hat)
    - Go 1.17
    - QEMU emulator version 6.2.0


## 3 Setup

You need to install all dependencies before doing anything else.

### 3.1 Packages

The following packages should be installed on your Linux distribution:

    - uuid
    - uuid-dev
    - make
    - build-essential
    - git
    - xz-utils
    - wget
    - python3
    - python
    - acpi
    - acpica-tools
    - flex
    - bison
    - bc

You should be able to install all of these using your package manager, like `apt` or `yum`. Some of these packages might have slightly different names on different distributions. For instance, if you are running Fedora, the `xz-utils` package is named `xz`.

### 3.2 Go

It is **highly** recommended that you use Go version 1.17. We provide a script for installing Go, [`InstallGo.sh`](Code/scripts/InstallGo.sh), that will uninstall all previous versions of Go on your system and download Go 1.17, and install it.

<details>
<summary>Click here for manual steps to install Go</summary>

If you wish not to use `InstallGo.sh` or want to understand how it works, here is a breakdown of the script.

Remove the previous versions of Go by running
```
$ sudo rm -rf /usr/local/go/
```
Then, download Go 1.17 and extract it to `/usr/local/go/`:
```
$ wget https://go.dev/dl/go1.17.linux-arm64.tar.gz
$ sudo tar -C /usr/local/ -xzf go1.17.linux-arm64.tar.gz
```
If you choose to install Go in a location other than `/usr/local/go/`, you will have to modify [`ImageBuildscript.sh`](Code/scripts/ImageBuildscript.sh) by changing the line `export PATH=/usr/local/go/bin/:$PATH` to `export PATH=<path-to-your-go-installation>/bin/:$PATH`.

</details>

**Caution:** If you wish to keep other versions of Go on your system, do not run [`InstallGo.sh`](Code/scripts/InstallGo.sh), as this script will uninstall everything before installing Go 1.17. Instead, see [here](https://go.dev/doc/manage-install) for having multiple versions of Go installed. If you choose to do this, you will have to modify the scripts so that `go` refers to `go1.17` instead of a different version.

Using a different version of Go is likely to produce problems. See [`u-root`](https://github.com/u-root/u-root) for details.

### 3.3 QEMU

To use the sbsa-ref QEMU machine you need QEMU version 4.1.0 or above. To identify what version you have, run:
```
$ <path-to-your-qemu-installation>/bin/qemu-system-aarch64 --version
```
The output should be this or similar:
```
QEMU emulator version 6.1.50 (v6.1.0-2172-gc88da1f3da)
Copyright (c) 2003-2021 Fabrice Bellard and the QEMU Project developers
```

If you don't have QEMU installed or don't have the right version, we provide a script to build it from source, [`InstallQemu.sh`](Code/scripts/InstallQemu.sh). You can skip this part if you do have an appropriate version of QEMU installed. Before running the script, you need to install the following packages:

    - git
    - make
    - build-essential
    - libglib2.0-dev
    - libfdt-dev
    - libpixman-1-dev
    - zlib1g-dev
    - ninja-build

Then, you can just navigate to `Code/scripts/` and run
```
./InstallQemu.sh
```

<details>
<summary>Click here for manual steps to install QEMU</summary>

If you wish not to use `InstallQemu.sh` or want to understand how it works, here is a breakdown of the script.

Set `INSTALL_PATH` to `/usr/local` or `$HOME/local` or your preferred location.
``` 
$ export INSTALL_PATH=/usr/local 
```
Run the following to clone and install QEMU at your `INSTALL_PATH`:
``` 
$ git clone https://github.com/qemu/qemu
$ cd qemu
qemu$ mkdir build-native
qemu$ cd build-native
build-native$ ../configure --target-list=aarch64-softmmu --prefix=$INSTALL_PATH
build-native$ make install
```
If you choose to install QEMU in a location other than `/usr/local/`, you will have to modify [`Run.sh`](Code/scripts/Run.sh) by changing the line `export INSTALL_PATH=/usr/local` to `export INSTALL_PATH=<path-to-your-qemu-installation>`.

</details>

### 3.4 Cross-compiler

If you are building Linux and EDK II on a system that isn't AArch64-based then you will need a suitable cross-compiler.

If on x86, download a cross compiler from [Linaro](http://releases.linaro.org/components/toolchain/binaries/6.2-2016.11/aarch64-linux-gnu/).

Add the cross-compiler to your `PATH` by adding the following line to `~/.bash_profile`:
```
$ export PATH=$PATH:<path/to/gcc-linaro-6.2.1-2016.11-x86_64_aarch64-linux-gnu/bin/>
```
and then running `source ~/.bash_profile`.

## 4 Building `initramfs`

Before building an `initramfs` using `u-root`, ensure the following is true:
- You have the correct packages installed (Section 3.1)
- You have the correct version of Go installed (Section 3.2)
- You have the correct version of QEMU installed (Section 3.3)
- If you are not running on AArch64, you have a cross-compiler installed (Section 3.4)
- [`bootmgrlib.patch`](Code/bootmgrlib.patch), [`uefishell.patch`](Code/uefishell.patch), and [`linuxboot_defconfig`](Code/linuxboot_defconfig) are located in the `Code/` directory. This should already be the case if you cloned the [repository from Gitlab](https://gitlab.arm.com/systemready/linuxboot-resources/-/tree/master/). In case they are not here, download them from the [repository](https://gitlab.arm.com/systemready/linuxboot-resources/-/tree/master/) and place them in the `Code/` directory.

Now, you can build by going to `Code/scripts/` and running
```
$ ./ImageBuildscript.sh
```

<details>
<summary>Click here for manual steps to build initramfs</summary>

If you wish not to use `ImageBuildscript.sh` or want to understand how it works, here is a breakdown of the script.

### 4.1 Downloading `u-root`

Once you have Go (Section 3.2), you can use it to download the `u-root` project. 
```
$ GO111MODULE=off go get github.com/u-root/u-root
```
We need to make sure that `GO111MODULE=off` because this is required by `u-root`. You should also add `u-root` to your `PATH`:
```
$ export PATH=$PATH:$HOME/go/bin
```

This info is condensed from [u-root github](https://github.com/u-root/u-root).

 

### 4.2 Building Ramdisk
Once we've downloaded `u-root`, we can use it to build a basic ramdisk like so.
```
$ GOARCH=arm64 GO111MODULE=off u-root -build bb core
```
The build should create `initramfs.linux_arm64.cpio` and let you know where the output directory is. You should copy it to your working directory and rename it `u-root.initramfs.linux_arm64.cpio` for clarity.

Once this is built we can move on to downloading and building Linux.


### 4.3 Linux

In order to compile our LinuxBoot image, we will pull straight from the mainline linux kernel and compile ourselves. This guide was tested with Linux kernel version 5.14 (commit 7d2a07b769330c34b4deabeed939325c77a7ec2f).

#### 4.3.1 Downloading Linux Kernel
Simply clone and check out v5.18 of the kernel.
```
$ git clone https://github.com/torvalds/linux
$ cd linux/
linux$ git checkout v5.18
```

#### 4.3.2 `defconfig`
We need to setup a `defconfig` for Linuxboot. We provide this in the repository as [`linuxboot_defconfig`](Code/linuxboot_defconfig).

Then we move it where the Linux build system expects it to be.
```
$ cp linuxboot_defconfig <path to>/linux/arch/arm64/configs/linuxboot_defconfig
```

Once we have our defconfig in the right place, we want to make sure that the path to our initramfs is right. Ensure the line with `CONFIG_INITRAMFS_SOURCE` reflects the path to your `u-root.initramfs.linux_arm64.cpio`. **Note:** Make sure to put quotes around this path. Here is a sample path:
```
CONFIG_INITRAMFS_SOURCE="/path/to/u-root.initramfs.linux_arm64.cpio"
```

#### 4.3.3 Building Linux
Once everything is set up, you can go ahead and build. The `-j $(nproc)` flag justs will build with as many parallel threads as there are processing units on your system.

On x86_64, run
```
linux$ mkdir -p out/linuxboot
linux$ make mrproper
linux$ make O=out/linuxboot/ CROSS_COMPILE=aarch64-linux-gnu- ARCH=arm64 linuxboot_defconfig
linux$ make O=out/linuxboot/ CROSS_COMPILE=aarch64-linux-gnu- ARCH=arm64 -j $(nproc) Image
```
If running on AArch64, you can remove the `CROSS_COMPILE` variable.

You should end up with an image at `linux/out/linuxboot/arch/arm64/boot/Image` which we will use to integrate into EDK2.

</details>

You should end up with an image at `linux/out/linuxboot/arch/arm64/boot/Image` which we will use to integrate into EDK II.

It is possible to automate the process of booting the final (stage-2) OS from LinuxBoot by adding an init script to the command that builds the `initramfs`. This isn't included in the build script, but details on how to add it are below.

<details>
<summary>Click here for details on adding init script to u-root</summary>

Create a shell script containing what you want to run immediately once u-root has booted. For the ACS image used in this guide, the following will work:

`automation-script.sh`
```
$ mount /dev/sda1 temp
$ cd temp/
$ kexec -i ramdisk-busybox.img -l Image -c "rootwait verbose debug crashkernel=256M"
$ kexec -e
```

Modify the line in the script that builds the initramfs so that it is as follows:

```
$ GOARCH=arm64 GO111MODULE=off u-root -build bb -files "<path-to>/automation-script.sh" -uinitcmd="/bin/sh /automation-script.sh" core
```
Rebuild the LinuxBoot image, and then recompile EDK II using the new image (section 4 in this guide).

</details>

## 5 Building EDK II

We use the `LinuxbootBootMgrLib` implementation ([see here for more details](https://github.com/tianocore/edk2/commit/7d28b879d33aa7803b6b15bd5fb3395526b891cf)) to incorporate LinuxBoot into EDK II. This method uses the [`bootMgr.patch`](Code/bootMgr.patch) patch.

Before building EDK II, make sure that you built the `initramfs` image (see Section 4).

Now, you can build EDK II by going to `Code/scripts/` and running
```
$ ./Edk2Buildscript.sh
```
The script will prompt you to choose which method of incorporating LinuxBoot into EDK II you want to use. To use `BootMgrLib`, enter 2. (The other method is covered in the drop-down section below.) You can also provide this on the command line to skip the prompt:
```
$ ./Edk2Buildscript.sh 2
```

The script assumes that you cloned the repository from `https://gitlab.arm.com/systemready/linuxboot-resources`. If this is not the case (e.g. you downloaded the files manually), the necessary patches might be in different locations. In that case, you should edit the lines `export PATCH="../../../uefishell.patch"` and `export PATCH="../../../bootmgrlib.patch"` to `export PATCH="<path-to-uefishell-patch>/uefishell.patch"` and `export PATCH="<path-to-bootmgrlib-patch>/bootmgrlib.patch"`, respectively.

<details>
<summary>Click here for an alternative way of incorporating LinuxBoot into EDK II</summary>

There are two ways to incorporate LinuxBoot into EDK II. The one we described above was the `LinuxbootBootMgrLib` implementation. The other one is replacing the UEFI shell. The process for building EDK II is similar in this case, except a different patch will be applied: for replacing the UEFI shell method, the patch used is [`linuxboot.patch`](Code/linuxboot.patch).

To use the UEFI shell replacement method, you can either enter 1 when prompted by `Edk2Buildscript`, or just run
```
$ ./Edk2Buildscript.sh 1
```

**We recommend using the `BootMgrLib` implementation.**

</details>



<details>
<summary>Click here for manual steps to build EDK II</summary>

If you wish not to use `Edk2Buildscript.sh` or want to understand how it works, here is a breakdown of the script.

Create an EDK2 Workspace.
```
$ mkdir -p <path to working directory>/sbsa-linuxboot
$ cd sbsa-linuxboot/
$ export WORKSPACE=$PWD
```
Clone EDK II and dependencies.
```
$ git clone git@github.com:tianocore/edk2.git
$ git clone git@github.com:tianocore/edk2-platforms.git
$ git clone git@github.com:tianocore/edk2-non-osi.git
```
Depending on the method you chose (replacing the UEFI shell or using `LinuxbootBootMgrLib`), you will need to apply different patches. For replacing the UEFI shell, run the following:
```
$ cd edk2-platforms/
$ git apply ../../../uefishell.patch
$ cd ../
```
For using `LinuxbootBootMgrLib`, run the following:
```
$ cd edk2-platforms/
$ git apply ../../../bootmgrlib.patch
$ cd ../
```
For more details on these patches, take a look at Section 7. Copy the image:
```
$ mkdir -p AArch64/
$ cp ../linux/out/linuxboot/arch/arm64/boot/Image ./AArch64/
```
Set up package path:
```
$ export PACKAGES_PATH=$WORKSPACE:$WORKSPACE/edk2:$WORKSPACE/edk2-platforms:$WORKSPACE/edk2-non-osi:$WORKSPACE/AArch64/
```
Set up build environment (the script has to be sourced to preserve the environment):
```
$ source edk2/edksetup.sh
```
Build Base Tools:
```
$ make -C edk2/BaseTools
```
Compile EDK2 for sbsa-qemu:
```
$ cd $WORKSPACE
$ GCC5_AARCH64_PREFIX=aarch64-linux-gnu- build -b RELEASE -a AARCH64 -t GCC5 -p edk2-platforms/Platform/Qemu/SbsaQemu/linuxboot/LbootSbsaQemu.dsc
```
You can omit the cross-compile prefix if running on AArch64. Here is a breakdown of the build command:

    GCC5_AARCH64_PREFIX=aarch64-linux-gnu- selects the right cross-compiler
    -b is build type (can be either DEBUG or RELEASE)
    -a is build architecture 
    -t toolchain version, GCC5 is really GCC5+, in our case GCC 8.3
    -p selects machine to compile EDK2

The build command produces two files, `SBSA_FLASH0.fd` and `SBSA_FLASH1.fd`, as output. Copy these to the top `$WORKSPACE` directory and extend to match the machine flash file size.
```
$ cp Build/SbsaQemu/RELEASE_GCC5/FV/SBSA_FLASH[01].fd .
$ truncate -s 256M SBSA_FLASH[01].fd
```

</details>


## 6 Running the emulator

Regardless of which method you used to incorporate LinuxBooth into EDK II, you can run QEMU by going to `Code/scripts/` and running:
```
$ ./Run.sh
```
With this command, you can boot only the first-stage kernel. If you wish to boot a second-stage kernel, you need to provide an image as an argument to `Run.sh`.

For instance, you can Boot the Linux portion of the Architecture Compliance Suite (ACS): Download your preferred image from [arm-systemready](https://github.com/ARM-software/arm-systemready) (select ES, there are links to pre-built releases from there), extract it, and pass it to `Run.sh`:
```
$ ./Run.sh es_acs_live_image.img
```

<details>
<summary>Click here for manual steps to run QEMU</summary>

If you wish not to use `Run.sh` or want to understand how it works, here is a breakdown of the script.

```
$ qemu-system-aarch64 -m 1024 -M sbsa-ref -pflash SBSA_FLASH0.fd -pflash SBSA_FLASH1.fd -nographic -hda </path/to/image.img>
```

Here is a breakdown of the run command:

    qemu-system-aarch64 the QEMU command to run using the aarch64 architecture
    -m sets the emulated machine's RAM in megabytes
    -M selects the board/machine to emulate, in this case the sbsa-ref machine
    -pflash specifies a file to use as a parallel flash image
    -nographic prevents QEMU from opening a window
    -hda specifies a file to use as a hard disk image

If you would prefer running in a separate window, change `-nographic` to `-serial stdio`.

</details>

If you used the `BootMgrLib` in Section 5, QEMU should automatically boot straight to the `u-root` userspace.

You can list the attached disks with `ls /dev/sd*`, and mount them. Once you've mounted your drive you can configure your kexec with `kexec -i <initrd> -l <VmLinux> -c 'root=<root path>' ` and once it's configured, you can execute with `kexec -e`.

In u-root command line mount the image and do a kexec call to the stage-2 linux kernel:
```
$ mkdir temp_dir
$ mount /dev/sda1 temp_dir
$ cd temp_dir
$ kexec -i ramdisk-busybox.img -l Image -c "rootwait verbose debug crashkernel=256M"
$ kexec -e
```

![Image: u-root console showing mount and kexec of the ACS image](DemoMedia/PlatformBootMgrDemo/kexecACSKernel.PNG)

Once the OS has booted, results from FWTS and other information can be found in `/mnt/acs_results`.

![Image: Screenshot of final test results from booting stage-2 kernel, and where to find the acs results](DemoMedia/PlatformBootMgrDemo/bootedToACS.PNG)

Now, the process of how to boot into the first-stage kernel depens on which method of incorporating LinuxBoot you are using.

<details>
<summary>Click here for instructions on running the emulator when UEFI shell replacement is used</summary>

If you replaced the UEFI shell in Section 5, you will need to do some additional steps.

Make sure to press the Esc key immediately when the trusted firmware has loaded (once you see the "NOTICE:BL31" messages have printed).


![Image: output from emulator starting showing the point at which the esc key should be pressed](DemoMedia/ReplaceUefiShell/StartingEmulator.png)

**NOTE:** If esc is not pressed the stage-2 OS will boot immediately, bypassing the linuxboot stage-1 kernel.

Use arrow keys to select `Boot Manager` and press Enter.
![Image: UEFI menu with Boot Manager highlighted as having been selected](DemoMedia/ReplaceUefiShell/UEFImenu.png)

Select the Shell and press Enter.

![Image: UEFI Boot Manager menu with UEFI shell highlighted as having been selected](DemoMedia/ReplaceUefiShell/UEFImenu2.png)

Now the stage-1 linuxboot kernel boots to uroot userspace.

Once LinuxBoot is started, you can list the attached disks with `ls /dev/sd*`, and mount them by creating a folder and mounting, for example:
```
$ mkdir temp
$ mount sda1 temp
```

![Image: u-root console showing contents of root directory](DemoMedia/ReplaceUefiShell/LBKernel1.png)


Once you've mounted your drive you can configure your kexec with `kexec -i <initrd> -l <VmLinux> -c 'root=<root path>' ` and once it's configured, you can execute with `kexec -e`.

In u-root command line mount the image and do a kexec call to the stage-2 linux kernel:
```
$ mkdir temp_dir
$ mount /dev/sda1 temp_dir
$ cd temp_dir
$ kexec -i ramdisk-busybox.img -l Image -c "rootwait verbose debug crashkernel=256M"
$ kexec -e
```

![Image: u-root console showing results of mounting sda1, displaying contents, navigating to mounted directory and running the first kexec command](DemoMedia/ReplaceUefiShell/LBKernel2.png)


Results from FWTS and other information can be found in `/mnt/acs_results`.

![Image: Screenshot of final test results from booting stage-2 kernel, and where to find the acs results](DemoMedia/ReplaceUefiShell/Stage2Os.png)

</details>


## 7 Code changes for LinuxBoot

There are patches to allow you to easily apply these changes to `edk2-platforms`, however they may not work seamlessly with updates to edk2 or edk2-platforms, so this section includes a description of the changes to help you make them yourself should this be required.

[Patch for replacing the UEFI Shell with Linuxboot](Code/uefishell.patch)

[Patch for using the LinuxBootBootMgrLib](Code/bootmgrlib.patch)

Our patches were tested on May 20, 2022 with the following releases/commits:
```
edk2: commit a21a3438f795deecb24e1843c1636f95c485017c
edk2-platforms: commit 3b896d1a325686de3942723c42f286090453e37a
ekd2-non-osi: commit 6996a45d7f4014fd4aa0f1eb4cbe97d8a3c5957a
linux: tag v5.18
```
Our scripts reference these commits explicitly. If you want to try newer commits, you can replace tags and commit hashes in the scripts.

<details>
<summary>Click here for manual steps to create patches</summary>

If you wish not to use our provided patches or want to understand how they work, here are the steps to create them.

To start, within your edk2-platforms repository you need to make a new package for your linuxboot files, and copy the .dsc and .fdf files used in the standard UEFI EDK2 build above (section 4.1) to the new directory. It is a good idea to rename them as well at this point. You also need to create a new file, call it linuxboot.inf.

```
$ mkdir -p $WORKSPACE/edk2-platforms/Platform/Qemu/SbsaQemu/linuxboot
$ cd $WORKSAPCE/edk2-platforms/Platform/Qemu/SbsaQemu
$ cp SbsaQemu.dsc ./linuxboot
$ cp SbsaQemu.fdf ./linuxboot
$ cd linuxboot
$ mv SbsaQemu.dsc LbootSbsaQemu.dsc
$ mv mv SbsaQemu.fdf LbootSbsaQemu.fdf
$ touch linuxboot.inf
```

So now you have three files in your linuxboot package directory. They are LbootSbsaQemu.dsc, LbootSbsaQemu.fdf, and linuxboot.inf. We will now describe the changes you need to make to each of these files. These changes will depend on the method of incorporating LinuxBoot that you choose to use. Section 7.1 describes the changes you need to make if you are replacing the UEFI shell. Section 7.2 describes the changes you need to make if you are using `BootMgrLib`.

### 7.1 Replacing UEFI Shell

#### 7.1.1 `LbootSbsaQemu.dsc`

Within the Defines section the line with FLASH_DEFINITION needs to be altered to point to your LbootSbsaQemu.fdf file rather than the SbsaQemu.fdf file.
```
FLASH_DEFINITION               = Platform/Qemu/SbsaQemu/linuxboot/LbootSbsaQemu.fdf
```

#### 7.1.2 `LbootSbsaQemu.fdf`

The Flash Descriptor File (fdf) describes where different parts of the firmware are stored in flash memory files. The linuxboot stage-1 kernel currently is significantly larger than the UEFI firmware shell that we are replacing, so space needs to be made for it, and other variables shifted so that we're not trying to use the same addresses for multiple things.

In the section called FD.SBSA_FLASH1, you need to change the size from 0x002C0000 to 0x009C0000, and the BlockSize x NumBlocks must equal the size, so change NumBlocks to 0x9C0.
```
[FD.SBSA_FLASH1]
BaseAddress   = 0x10000000|gArmTokenSpaceGuid.PcdFdBaseAddress
Size          = 0x009C0000|gArmTokenSpaceGuid.PcdFdSize
ErasePolarity = 1
BlockSize     = 0x00001000
NumBlocks     = 0x9C0
```

The LinuxBoot kernel is expanding the UEFI image size, "Place for EFI (BL33)", so that section needs to be expanded to 0x008FF000.
```
## Place for EFI (BL33)
# This offset (if any as it is 0x0 currently) + BaseAddress (0x10000000) must be set in PRELOADED_BL33_BASE at ATF
0x00000000|0x008FF000
gArmTokenSpaceGuid.PcdFvBaseAddress|gArmTokenSpaceGuid.PcdFvSize
FV = FVMAIN_COMPACT
```

The variables need to be shifted so that they start after the LinuxBoot kernel but still aligned to the flash block size, so start them at 0x00900000. There are three sets of them, each of size 0x40000. The lines where they are defined as of the time of writing of this guide are line 74, line 112, and line 126. These line numbers may change slightly in case of updates to edk2 or edk2-platforms, but should serve as a guide and there is context to each of the lines that need to be changed below.
```
## Place for Variables. They share flash1 with EFI
# Must be aligned to Flash Block size 0x40000
0x00900000|0x00040000   #Line 74

...

0x00940000|0x00040000   #Line 112
gEfiMdeModulePkgTokenSpaceGuid.PcdFlashNvStorageFtwWorkingBase|gEfiMdeModulePkgTokenSpaceGuid.PcdFlashNvStorageFtwWorkingSize
#NV_FTW_WORKING

...

0x00980000|0x00040000   #Line 126
gEfiMdeModulePkgTokenSpaceGuid.PcdFlashNvStorageFtwSpareBase|gEfiMdeModulePkgTokenSpaceGuid.PcdFlashNvStorageFtwSpareSize
#NV_FTW_SPARE
```

The final change to the fdf is to replace the inclusion of the UEFI shell package (ShellPkg/Application/Shell/Shell.inf) with the linuxboot package (edk2-platforms/Platform/Qemu/SbsaQemu/linuxboot/linuxboot.inf).

```
  #
  # UEFI application (Shell Embedded Boot Loader)
  #
  INF edk2-platforms/Platform/Qemu/SbsaQemu/linuxboot/linuxboot.inf
```

#### 7.1.3 `linuxboot.inf`

Populate the linuxboot.inf file with the following text. The FILE_GUID is the same as that of the shell package you've replaced in the .fdf file, and the Binaries section points to the linuxboot stage-1 image (as generated in section 3.3 of this guide).

```
[Defines]
    INF_VERSION             = 0x00010006
    BASE_NAME               = LinuxBoot
    FILE_GUID               = 7C04A583-9E3E-4f1c-AD65-E05268D0B4D1
    MODULE_TYPE             = UEFI_APPLICATION
    VERSION_STRING          = 1.0

[Binaries.AArch64]
    PE32|AArch64/Image|*
```

### 7.2 Using the Platform Boot Manager Library for Linuxboot

#### 7.2.1 `LbootSbsaQemu.dsc`

Within the Defines section the line with FLASH_DEFINITION needs to be altered to point to your LbootSbsaQemu.fdf file rather than the SbsaQemu.fdf file.
```
FLASH_DEFINITION               = Platform/Qemu/SbsaQemu/linuxboot/LbootSbsaQemu.fdf
```

Then change the line defining the PlatformBootManagerLib (line 183 as of the time of writing) to point to the LinuxBootBootManagerLib. The new line should be:
```
PlatformBootManagerLib|ArmPkg/Library/LinuxBootBootManagerLib/LinuxBootBootManagerLib.inf
```

Add the section below to the "Pcd Section - list of all EDK II PCD Entries defined by this Platform".

```
[PcdsDynamicExDefault]
  gArmTokenSpaceGuid.PcdLinuxBootFileGuid|{ 0x47, 0x15, 0xA9, 0x1F, 0x23, 0xDB, 0x6A, 0x4F, 0x8A, 0xF8, 0x3B, 0x97, 0x82, 0xA7, 0xF9, 0x17 }
```

#### 7.2.2 `LbootSbsaQemu.fdf`

See section [6.1.2](#612-lbootsbsaqemufdf) for details.

#### 7.2.3 `linuxboot.inf`

Populate the linuxboot.inf file with the following text. The FILE_GUID is from the Linuxboot Boot Manager Library (can be found in edk2/ArmPkg/Library/LinuxBootBootManagerLib/). The Binaries section points to the linuxboot stage-1 image (as generated in section 3.3 of this guide).

```
[Defines]
    INF_VERSION             = 0x00010006
    BASE_NAME               = LinuxBoot
    FILE_GUID               = 1FA91547-DB23-4F6A-8AF8-3B9782A7F917
    MODULE_TYPE             = UEFI_APPLICATION
    VERSION_STRING          = 1.0

[Binaries.AArch64]
    PE32|AArch64/Image|*
```

</details>

## 8 Removing DXEs 

The Driver eXecution Environment phase (DXE) is a part of the process of booting a system, and is largely something that linuxboot aims to replace. Many of the DXEs that are included in the edk2-platforms implementation for sbsa-ref QEMU are not required. 

The following DXEs can be removed:
```
MdeModulePkg/Universal/Console/ConPlatformDxe/ConPlatformDxe.inf
MdeModulePkg/Universal/Console/GraphicsConsoleDxe/GraphicsConsoleDxe.inf
MdeModulePkg/Universal/Console/TerminalDxe/TerminalDxe.inf
MdeModulePkg/Universal/SerialDxe/SerialDxe.inf
MdeModulePkg/Universal/Disk/DiskIoDxe/DiskIoDxe.inf
MdeModulePkg/Universal/Disk/PartitionDxe/PartitionDxe.inf
MdeModulePkg/Universal/Disk/UnicodeCollation/EnglishDxe/EnglishDxe.inf
FatPkg/EnhancedFatDxe/Fat.inf
MdeModulePkg/Universal/Disk/UdfDxe/UdfDxe.inf
MdeModulePkg/Universal/DisplayEngineDxe/DisplayEngineDxe.inf
MdeModulePkg/Universal/SetupBrowserDxe/SetupBrowserDxe.inf
MdeModulePkg/Universal/DriverHealthManagerDxe/DriverHealthManagerDxe.inf
MdeModulePkg/Application/UiApp/UiApp.inf
MdeModulePkg/Bus/Scsi/ScsiBusDxe/ScsiBusDxe.inf
MdeModulePkg/Bus/Scsi/ScsiDiskDxe/ScsiDiskDxe.inf
MdeModulePkg/Universal/Acpi/BootGraphicsResourceTableDxe/BootGraphicsResourceTableDxe.inf
MdeModulePkg/Bus/Usb/UsbKbDxe/UsbKbDxe.inf
MdeModulePkg/Logo/LogoDxe.inf
MdeModulePkg/Universal/Disk/RamDiskDxe/RamDiskDxe.inf
MdeModulePkg/Bus/Usb/UsbBusDxe/UsbBusDxe.inf
OvmfPkg/SataControllerDxe/SataControllerDxe.inf
MdeModulePkg/Bus/Ata/AtaBusDxe/AtaBusDxe.inf
MdeModulePkg/Bus/Scsi/ScsiBusDxe/ScsiBusDxe.inf
MdeModulePkg/Bus/Scsi/ScsiDiskDxe/ScsiDiskDxe.inf
MdeModulePkg/Bus/Ata/AtaAtapiPassThru/AtaAtapiPassThru.inf
MdeModulePkg/Bus/Pci/UhciDxe/UhciDxe.inf
MdeModulePkg/Bus/Pci/EhciDxe/EhciDxe.inf
MdeModulePkg/Bus/Pci/XhciDxe/XhciDxe.in
NetworkPkg/Network.fdf.inc 
SecurityPkg/VariableAuthenticated/SecureBootConfigDxe/SecureBootConfigDxe.inf
OvmfPkg/QemuVideoDxe/QemuVideoDxe.inf
```
