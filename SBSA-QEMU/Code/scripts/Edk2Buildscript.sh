#!/usr/bin/env bash

# Copyright (c) 2021, ARM Limited and Contributors. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.
#
# Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.
#
# Neither the name of ARM nor the names of its contributors may be used
# to endorse or promote products derived from this software without specific
# prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

# Setting up environment
mkdir -p sbsa-linuxboot/
cd sbsa-linuxboot/
export WORKSPACE=$PWD 

# Download edk2 and edk2-platforms if needed
if [ ! -d edk2/ ]; then
    echo "$0: Cloning edk2, this might take a while..."
    git clone https://github.com/tianocore/edk2.git
    cd edk2/
    git checkout a21a3438f795deecb24e1843c1636f95c485017c
    git submodule update --init
    cd ../
else
    echo "$0: Found edk2"
fi
if [ ! -d edk2-platforms/ ]; then
    echo "$0: Cloning edk2-platofm, this might take a while..."
    git clone https://github.com/tianocore/edk2-platforms.git
    cd edk2-platforms/
    git checkout 3b896d1a325686de3942723c42f286090453e37a
    cd ../
else
    echo "$0: Found edk2-platforms"
    # Clean up the previous config
    rm -rf edk2-platforms/Platform/Qemu/SbsaQemu/linuxboot/
fi
if [ ! -d edk2-non-osi/ ]; then
    echo "$0: Cloning edk2-non-osi, this might take a while..."
    git clone https://github.com/tianocore/edk2-non-osi.git
    cd edk2-non-osi/
    git checkout 6996a45d7f4014fd4aa0f1eb4cbe97d8a3c5957a
    cd ../
else
    echo "$0: Found edk2-non-osi"
fi


export PACKAGES_PATH=$WORKSPACE:$WORKSPACE/edk2:$WORKSPACE/edk2-platforms:$WORKSPACE/edk2-non-osi

# Decide between replacing UEFI shell and using BootMgrLib
first_try=1
while true; do
    if ((first_try)) && [[ $# -gt 0 ]]; then
        choice="$1"
        first_try=0
    else
        echo "$0: Please enter the number of a method of incorporating LinuxBoot"
        echo "$0: [1] Replacing UEFI shell"
        echo "$0: [2] BootMgrLib"
        echo "$0: [0] Exit this script"
        read -p "$0: Enter a number: " choice
    fi
    case "$choice" in
        1 ) echo "$0: Using UEFI shell replacement method";
                    export PATCH="../../../uefishell.patch";
                    break;;
        2 ) echo "$0: Using BootMgrLib method";
                    export PATCH="../../../bootmgrlib.patch";
                    break;;
        0 ) echo "$0: Exiting";
            exit;;
        * ) echo "$0: Invalid choice of method: $choice";
    esac
done
echo "$0: Applying LinuxBoot patch"
cd edk2-platforms/
git apply --whitespace=nowarn "$PATCH"
cd ../

# Copy the LinuxBoot image to AArch64 directory and add to PACKAGES_PATH 
mkdir -p AArch64/
cp ../linux/out/linuxboot/arch/arm64/boot/Image ./AArch64/

# Set up tools and environment for edk2 build
echo "$0: Building environment for EDK II, this might take a while..."
make -C edk2/BaseTools/
# Unset positional arguments
set --
source edk2/edksetup.sh

# Set cross-compile prefix if not running on AArch64
if [[ $(uname -m) != "aarch64" ]]; then
    export GCC5_AARCH64_PREFIX=aarch64-linux-gnu-
fi

# Build edk2 using the LinuxBoot sbsa-qemu .dsc file
echo "$0: Building EDK II, this might take a while..."
build -b RELEASE -a AARCH64 -t GCC5 -p edk2-platforms/Platform/Qemu/SbsaQemu/linuxboot/LbootSbsaQemu.dsc

# A breakdown of the build command:
#
#    GCC5_AARCH64_PREFIX=aarch64-linux-gnu- selects the right cross-compiler
#    -b is build type (can be either DEBUG or RELEASE)
#    -a is build architecture 
#    -t toolchain version, GCC5 is really GCC5+, in our case GCC 8.3
#    -p selects machine to compile EDK2

# Copy output of build to parent directory and expand to match the machine flash size
echo "$0: Copying output"
cp Build/SbsaQemu/RELEASE_GCC5/FV/SBSA_FLASH[01].fd ../
truncate -s 256M ../SBSA_FLASH[01].fd

if [ $? -eq 0 ]; then
    echo "$0: Done!"
fi
