#!/usr/bin/env bash

# Copyright (c) 2021, ARM Limited and Contributors. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.
#
# Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.
#
# Neither the name of ARM nor the names of its contributors may be used
# to endorse or promote products derived from this software without specific
# prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.


# Used to find the ramdisk, which is needed when configuring the Linux build.
# The assumption is that this script will be run from the same directory where the
# ramdisk is stored.
export WORKSPACE=$PWD

export PATH=/usr/local/go/bin/:$PATH

if ! command -v go >/dev/null 2>&1; then
    echo "$0: You need to install Go 1.17"
    echo "$0: Visit https://go.dev/dl/"
    exit 1
fi

if [[ $(go version | sed -n 's/.*go version \(.*\) .*/\1/p') != "go1.17" ]]; then
    echo "$0: Warning: You are not using Go 1.17, which is the recommended version"
    echo "$0: It is recommended that you install Go 1.17 first"
    while true; do
        read -p "$0: Are you sure you want to proceed? [y/n] " ans
        case $ans in
            y ) echo "$0: Proceeding" && break;;
            n ) exit;;
            * ) echo "$0: Answer y or n (case-sensitive)";;
        esac
    done
fi

# Install u-root
echo "$0: Installing u-root"
GO111MODULE=off go get github.com/u-root/u-root
export PATH=$PATH:$HOME/go/bin

#Generate initramfs and copy to workspace
echo "$0: Building initramfs"
GOARCH=arm64 GO111MODULE=off u-root -build=bb core
cp /tmp/initramfs.linux_arm64.cpio ./u-root.initramfs.linux_arm64.cpio

#Clone linux
if [ ! -d linux/ ]; then
    echo "$0: Cloning Linux, this might take a while..."
    git clone https://github.com/torvalds/linux
    cd linux/
else
    echo "$0: Found Linux"
    cd linux/
    git clean -fxd
fi
git checkout v5.18

# Copy LinuxBoot config and edit the CONFIG_INITRAMFS_SOURCE line
echo "$0: Configuring compile-time kernel options"
cp ../../linuxboot_defconfig arch/arm64/configs/
sed -i "s|^CONFIG_INITRAMFS_SOURCE=.*$|CONFIG_INITRAMFS_SOURCE=\"${WORKSPACE}/u-root.initramfs.linux_arm64.cpio\"|" arch/arm64/configs/linuxboot_defconfig

# Set cross-compile prefix if not running on AArch64
if [[ $(uname -m) != "aarch64" ]]; then
    export CROSS_COMPILE=aarch64-linux-gnu-
fi

# Compile the kernel
echo "$0: Compiling the kernel, this might take a while..."
rm -rf out/linuxboot/
mkdir -p out/linuxboot/
make O=out/linuxboot/ ARCH=arm64 linuxboot_defconfig
make O=out/linuxboot/ ARCH=arm64 -j $(nproc) Image

if [ $? -eq 0 ]; then
    echo "$0: Done!"
fi
