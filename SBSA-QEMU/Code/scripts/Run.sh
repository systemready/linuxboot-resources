#!/usr/bin/env bash

# Copyright (c) 2022, ARM Limited and Contributors. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.
#
# Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.
#
# Neither the name of ARM nor the names of its contributors may be used
# to endorse or promote products derived from this software without specific
# prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

export INSTALL_PATH=/usr/local

# Optionally, if an argument is given, run that image.
if [[ $# -gt 0 ]]; then
    if [[ ! -f "$1" ]]; then
        echo "$0: $1 not found"
        # Special case: if the argument is ES ACS image, offer to download it
        if [[ "$1" == "es_acs_live_image.img" ]]; then
            echo "$0: It looks like you are trying to use the ES Architecture Compliance Suite"
            while true; do
                read -p "$0: Download es_acs_live_image.img from Github? [y/n] " ans
                case $ans in
                    y ) echo "$0: Downloading es_acs_live_image.img.xz";
                        wget https://raw.githubusercontent.com/ARM-software/arm-systemready/main/ES/prebuilt_images/v21.09_1.0/es_acs_live_image.img.xz;
                        echo "$0: Unzipping es_acs_live_image.img.xz";
                        unxz es_acs_live_image.img.xz;
                        break;;
                    n ) exit;;
                    * ) echo "$0: Answer y or n (case-sensitive)";;
                esac
            done
        else
            exit
        fi
    fi
    export HDA="-hda $1"
else
    export HDA=""
fi

# `set -x` will explicitly print the command that we run
(set -x; $INSTALL_PATH/bin/qemu-system-aarch64 \
    -m 1024 \
    -M sbsa-ref \
    -drive file=SBSA_FLASH0.fd,format=raw,if=pflash \
    -drive file=SBSA_FLASH1.fd,format=raw,if=pflash \
    -nographic $HDA)
